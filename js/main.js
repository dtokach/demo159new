//svg
(function(f,l,m,e,g,n){function h(a,b){if(b){var c=b.getAttribute("viewBox"),d=f.createDocumentFragment(),e=b.cloneNode(!0);for(c&&a.setAttribute("viewBox",c);e.childNodes.length;)d.appendChild(e.childNodes[0]);a.appendChild(d)}}function p(){var a=f.createElement("x"),b=this.s;a.innerHTML=this.responseText;this.onload=function(){b.splice(0).map(function(b){h(b[0],a.querySelector("#"+b[1].replace(/(\W)/g,"\\$1")))})};this.onload()}function k(){for(var a;a=l[0];)if(g){var b=new Image;b.src=a.getAttribute("xlink:href").replace("#",
".").replace(/^\./,"")+".png";a.parentNode.replaceChild(b,a)}else{var b=a.parentNode,c=a.getAttribute("xlink:href").split("#"),d=c[0],c=c[1];b.removeChild(a);if(d.length){if(a=e[d]=e[d]||new XMLHttpRequest,a.s||(a.s=[],a.open("GET",d),a.onload=p,a.send()),a.s.push([b,c]),4===a.readyState)a.onload()}else h(b,f.getElementById(c))}m(k)}(g||n)&&k()})(document,document.getElementsByTagName("use"),window.requestAnimationFrame||window.setTimeout,{},/MSIE\s[1-8]\b/.test(navigator.userAgent),/Trident\/[567]\b/.test(navigator.userAgent)||
537>(navigator.userAgent.match(/AppleWebKit\/(\d+)/)||[])[1],document.createElement("svg"),document.createElement("use"));


$('.invest_tabs .tab').click(function() {
    $('.invest_tabs .tab').removeClass('active');
    $(this).addClass('active');
    var id = $(this).attr('id');
    $('.inv').fadeOut( 0, function() { });
    $('.inv.id_' + id).fadeIn( 600, function() { });
});

/* Копирование ссылки при клике на кнопку */

$('#copybtn').click(function () {
    copy();
});

function copy() {
    var cutTextarea = document.querySelector('#copy');
    cutTextarea.select();
    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copy');
    } catch (err) {
        console.log('Oops, unable to cut');
    }
}

$(document).ready(function(){
    $("#js-amount").on("change keyup", function () {

        calc($(this).val());
    });
    $("select[name='plan']").on("change", function () {
        calc($("#js-amount").val());
    });
    $(document).ready(function () {
        calc($("#js-amount").val());
    });

    function calc(amount) {
        var plans = <?php echo json_encode(get_plans(), true); ?>,
            plan_id = Number($("select[name='plan']").val()),
            percent = 0,
            count = 0,
            seconds = 0,
            plan_return = 0,
            check = false,
            min = 0,
            max = 0;

        if (plans) {
            plans.forEach(function (plan) {
                if (amount >= Number(plan["min"]) && amount <= Number(plan["max"]) && plan_id === Number(plan["id"])) {
                    count = Number(plan["count"]);
                    percent = Number(plan["percent"]);
                    seconds = Number(plan["seconds"]);
                    plan_return = Number(plan["return"]);
                    check = true;
                }
                max = Number(plan["max"]);
                min = Number(plan["min"]);
            });
        }

        if(count != 0){
            $("#srok").html(count+' <?php echo T('дн.'); ?>');

        }
        else{
            $("#srok").html(' <?php echo T('Бессрочно '); ?>');
        }
        $("#value").html(amount);
        $("#per_day").html(percent+"%");
        $("#min_gh_sum").html(min);
        $("#max_gh_sum").html(max);
        if (check) {
            var total = (amount / 100 * percent) * (count === 0 ? 1 : count) + Number(plan_return === 1 ? amount : 0);
            $("#sum1").html(total.toFixed(2));
        } else {
            $("#sum1").html("-----");
        }
    }
});

/* Инициализация WOW эффектов */

new WOW().init();